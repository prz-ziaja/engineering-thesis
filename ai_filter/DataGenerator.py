import h5py
import numpy as np
import tensorflow.keras as K

class DataGenerator(K.utils.Sequence):
    'Generates data for Keras'
    def __init__(self,dbPath,proportrion=0.5, batch_size=32768):
        'Initialization'
        self.db = h5py.File(dbPath,'r')
        self.numParticles = self.db["labels"].shape[0]
        self.stats = self.db["stats"][:,:]
        self.batch_size = batch_size
        self.proportrion = proportrion
        self.true_size = int(batch_size*proportrion)
        self.fake_size = batch_size-self.true_size
        self.on_epoch_end()

    def __len__(self):
        'Denotes the number of batches per epoch'
        return int((self.proportrion+1)*self.numParticles)

    def generator(self):
        for i in range(0, self.numParticles-self.batch_size, self.true_size):
            particles = self.db["particles"][i: i + self.true_size]
            particles = (particles - self.db["stats"][0,:])/self.db["stats"][1,:]
            particles = particles[:,[0,1,2,5,6,7]]

            random = np.random.uniform(-8,8,[self.fake_size,6])

            labels = np.ones(self.batch_size)
            labels[self.fake_size:] = 0
            labels = labels.reshape((-1,1))
            x = np.concatenate([particles,random],axis=0)

            yield x, labels
        
    def close(self):
        self.db.close()
