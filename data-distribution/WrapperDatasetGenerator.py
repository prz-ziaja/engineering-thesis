import os
import numpy as np
from HDF5DatasetGenerator import HDF5DatasetGenerator

class WrapperDatasetGenerator:
    def __init__(self,pwd,batch_size):
        self.pwd = pwd
        self.batch_size = batch_size
        self.generators = []
        for i in os.listdir(self.pwd):
            if 'DISP_' in i:
                _,dispersion,_,angle = i.split('_')
                subdir = os.path.join(self.pwd,i,'HDF5')
                for j in os.listdir(subdir):
                    if 'TrainGenerator.hdf5' in j:
                        energy = (ord(j[0]) - ord('a'))*0.2
                        size_of_source = int(j[1])/10
                        path_to_file = os.path.join(subdir,j)
                        generator = HDF5DatasetGenerator(path_to_file, self.batch_size)
                        _params = [dispersion,angle,energy,size_of_source]
                        params = [np.float32(param) for param in _params]
                        params = np.array(params).reshape((1,-1))
                        self.generators.append([params,generator])

    def generator(self):
        it = [(i,j.generator()) for i,j in self.generators]
        cond = True
        while cond:
            i = np.random.randint(0,len(it))
            try:
                yield it[i][0], next(it[i][1])
            except StopIteration:
                it.pop(i)
            except:
                return

    def close(self):
        for _, gen in self.generators:
            gen.close()

