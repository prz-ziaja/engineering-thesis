import numpy as np
from matplotlib import pyplot as plt
import gaga
from HDF5DatasetGenerator import HDF5DatasetGenerator
import matplotlib.pyplot as plt
import numpy as np



def gaga_plot(phsp_filename='output', pth_filename='output', n=7e4, nb_bins=100,
              toggle=None, radius=None, quantile=0.5, plot2d=None, epoch=-1, no_title=True):
    """
    \b
    Plot marginal distributions from a GAN-PHSP

    \b
    <PHSP_FILENAME>   : input phase space file PHSP file (.npy)
    <PTH_FILENAME>    : input GAN PTH file (.pth)
    """

    # nb of values
    n = int(n)

    keys_2d = plot2d
    if keys_2d is None:
        keys_2d = []

    # load data
    print("Loading data")
    particle_generator = HDF5DatasetGenerator('a1TrainGenerator.hdf5', batchSize=n)
    print("Loading data 2 ")
    real = next(particle_generator.generator())
    keys = ['E','X','Y','dX','dY','dZ']#['E','X','Y','angle1','angle2','dX','dY','dZ']

    # load pth
    print("Loading model")
    params, G, D, optim, dtypef = gaga.load(pth_filename, epoch=epoch)

    # generate samples
    fake = gaga.generate_samples2(params, G, n, n, True, True)
    print(fake.shape)
    print(real.shape)
    fig,axs = plt.subplots(2,3)
    for i, j in enumerate(keys):
        axs.flatten()[i].hist(real[:,i],range=(-3,3),bins=100,alpha=0.5)
        axs.flatten()[i].hist(fake[:,i],range=(-3,3),bins=100,alpha=0.5)    
        axs.flatten()[i].set_title(j)
    plt.legend(['real','fake'])

    plt.show()


# --------------------------------------------------------------------------
if __name__ == '__main__':
    gaga_plot()
