import json
import time
import socket
import gaga
import copy
import numpy as np
import colorama
from colorama import Fore, Style
import torch
import datetime
import sys
from HDF5DatasetGenerator import HDF5DatasetGenerator


def gaga_train(phsp_filename='input',
               output_filename='output_not_normalized',
               json_filename='config_001.json'):
    '''
    \b
    Train GAN to learn a PHSP (Phase Space File)
    \b
    <PHSP_FILENAME>   : input PHSP file (.npy)
    <JSON_FILENAME>   : input json file with all GAN parameters
    <OUTPUT_FILENAME> : output GAN as pth file
    '''

    """    # term color
    colorama.init()"""

    # read parameters
    param_file = open(json_filename).read()
    params = json.loads(param_file)

    """# add params from the command line
    params['progress_bar'] = progress_bar
    params['params_filename'] = json_filename
    params['training_filename'] = phsp_filename
    start = datetime.datetime.now()
    params['start date'] = start.strftime(gaga.date_format)
    params['hostname'] = socket.gethostname()
    params['start_pth'] = start_pth"""

    # read input training dataset
    #print(Fore.CYAN + "Loading training dataset ... " + phsp_filename + Style.RESET_ALL)
    #x, read_keys, m = phsp.load(phsp_filename)

    # consider only some keys
    #keys, x = gaga.select_keys(x, params, read_keys)

    # add information
    #params['training_size'] = len(x)
    #params['keys'] = keys
    #params['x_dim'] = len(keys)
    params['x_dim'] = 6
    params['training_size'] = 25
    params['loss_type'] = 'wasserstein'
    params['penalty_type'] = "gradient_penalty"
    params['penalty_weight'] = 0.1
    # print parameters
    for e in params:
        if (e[0] != '#'):
            print('   {:22s} {}'.format(e, str(params[e])))

    # COMPATIBILITY FIXME TO REMOVE ##"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    # COMPATIBILITY FIXME TO REMOVE ##"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    # COMPATIBILITY FIXME TO REMOVE ##"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    params['validation_filename'] = None
    params['plot'] = None
    params['validation_every_epoch'] = 0
    # COMPATIBILITY FIXME TO REMOVE ##"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    # COMPATIBILITY FIXME TO REMOVE ##"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    # COMPATIBILITY FIXME TO REMOVE ##"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    # build the model
    print(Fore.CYAN + 'Building the GAN model ...' + Style.RESET_ALL)
    gan = gaga.Gan(params)
    #print(params)
    #sys.exit()
    # train
    print(Fore.CYAN + 'Loading Data ...' + Style.RESET_ALL)
    particle_generator = HDF5DatasetGenerator('a1TrainGenerator.hdf5', batchSize=params['batch_size'])

    print(Fore.CYAN + 'Start training ...' + Style.RESET_ALL)


    optim = gan.train(particle_generator)

    # stop timer
    stop = datetime.datetime.now()
    params['end date'] = stop.strftime(gaga.date_format)

    # save output
    gan.save(optim, output_filename)

    print('done')


# --------------------------------------------------------------------------
if __name__ == '__main__':
    gaga_train()
