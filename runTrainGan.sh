#!/bin/bash -l

## Nazwa zlecenia
#SBATCH -J PZ_GAN_TEST_job

## Liczba alokowanych węzłów
#SBATCH -N 1

## Liczba zadań per węzeł (domyślnie jest to liczba alokowanych rdzeni na węźle)
#SBATCH --ntasks-per-node=1

## Ilość pamięci przypadającej na jeden rdzeń obliczeniowy (domyślnie 5GB na rdzeń)
#SBATCH --mem-per-cpu=25GB

## Maksymalny czas trwania zlecenia (format HH:MM:SS)
#SBATCH --time=001:00:00 

## Nazwa grantu do rozliczenia zużycia zasobów
#SBATCH -A plg3ddose

## Specyfikacja partycji
#SBATCH -p plgrid-gpu
#SBATCH --gres=gpu:2

## Plik ze standardowym wyjściem
#SBATCH --output="output_0.out"

## Plik ze standardowym wyjściem błędów
#SBATCH --error="error_0.err"


## przejscie do katalogu z ktorego wywolany zostal sbatch
cd $SLURM_SUBMIT_DIR

srun /bin/hostname

export LD_LIBRARY_PATH=/net/people/plgziajap/cuda/lib64:$LD_LIBRARY_PATH


source /net/people/plgziajap/venv/gans/bin/activate


python3 /net/people/plgziajap/engineering-thesis/src/e01/main.py





