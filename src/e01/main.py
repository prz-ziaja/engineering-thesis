import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
import datetime
import os, sys
import matplotlib.pyplot as plt
import h5py
import mlflow
import mlflow.pytorch
from scipy.stats import wasserstein_distance
from HDF5DatasetGenerator import HDF5DatasetGenerator

repo_pwd = __file__
for i in range(3):
    repo_pwd = os.path.dirname(repo_pwd)

mlflow.set_tracking_uri(os.path.join(repo_pwd,'mlruns'))

DEVICE = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
print(DEVICE)

file = os.path.join(repo_pwd,'data/a1TrainGenerator.hdf5')

# Batch size during training
batch_size = 1024*124

# Number of channels - generated parameters
nc = 6

# Size of z latent vector (i.e. size of generator input)
n_noise = 200

# Number of training epochs
num_epochs = 2

# Learning rate for optimizers
lr = 0.0002

# Beta1 hyperparam for Adam optimizers
beta1 = 0.35

keys = ['E', 'X', 'Y', 'dX', 'dY', 'dZ']

class Discriminator(nn.Module):

    def __init__(self, in_channel=1, input_size=784, condition_size=10, num_classes=1):
        super(Discriminator, self).__init__()
        self.fc = nn.Sequential(
            nn.Linear(nc, 64),
            nn.LayerNorm(64),
            nn.ReLU(),
            nn.Linear(64, 64),
            nn.LayerNorm(64),
            nn.ReLU(),
            nn.Linear(64, 64),
            nn.LayerNorm(64),
            nn.Sigmoid(),
            nn.Linear(64, 64),
            nn.LayerNorm(64),
            nn.Sigmoid(),
            nn.Linear(64, 1),
            nn.Sigmoid()
        )

    def forward(self, x):
        y_ = self.fc(x)
        return y_


class Generator(nn.Module):

    def __init__(self, input_size=100, condition_size=10):
        super(Generator, self).__init__()
        self.fc = nn.Sequential(
            nn.Linear(input_size, 32),
            nn.ReLU(),
            nn.LayerNorm(32),
            nn.Linear(32, 64),
            nn.ReLU(),
            nn.LayerNorm(64),
            nn.Linear(64, 128),
            nn.ReLU(),
            nn.Linear(128, 256),
            nn.ReLU(),
            nn.Linear(256, 512),
            nn.ReLU(),
            nn.Linear(512, 1024),
            nn.ReLU(),
            nn.Linear(1024, nc)

        )

    def forward(self, x):
        y_ = self.fc(x)
        return y_

generator = HDF5DatasetGenerator(file,batch_size)
D = Discriminator().to(DEVICE)
G = Generator(n_noise).to(DEVICE)
D_opt = torch.optim.RMSprop(D.parameters(), lr=0.0005)
G_opt = torch.optim.RMSprop(G.parameters(), lr=0.0005)
step = 0
g_step = 0

D_labels = torch.ones([batch_size, 1]).to(DEVICE) # Discriminator Label to real
D_fakes = torch.zeros([batch_size, 1]).to(DEVICE) # Discriminator Label to fake
mlflow.set_experiment("GAN-experiments")
with mlflow.start_run():
    for epoch in range(num_epochs):
        ds = generator.generator()
        # For each batch in the dataloader
        for step, data_raw in enumerate(ds):
            data = torch.tensor(data_raw)
            # Training Discriminator
            x = data.to(DEVICE)

            x_outputs = D(x)

            z = torch.randn(batch_size, n_noise).to(DEVICE)
            z_outputs = D(G(z))
            D_x_loss = torch.mean(x_outputs)
            D_z_loss = torch.mean(z_outputs)
            D_loss = D_z_loss - D_x_loss

            D.zero_grad()
            D_loss.backward()
            D_opt.step()
            # Parameter(Weight) Clipping for K-Lipshitz constraint
            for p in D.parameters():
                p.data.clamp_(-0.01, 0.01)

            if step % 1 == 0:
                g_step += 1
                # Training Generator
                z = torch.randn(batch_size, n_noise).to(DEVICE)
                z_outputs = D(G(z))
                G_loss = -torch.mean(z_outputs)

                D.zero_grad()
                G.zero_grad()
                G_loss.backward()
                G_opt.step()

            if step % 100 == 0:
                print('Epoch: {}/{}, Step: {}, D Loss: {}, G Loss: {}'.format(epoch, num_epochs, step, D_loss.item(),
                                                                              G_loss.item()))
                for num, key in enumerate(keys):
                    real = next(generator.generator())
                    z = torch.randn(batch_size, n_noise).to(DEVICE)
                    fake = G(z).to(torch.device("cpu")).detach().numpy()
                    mlflow.log_metric(f"Wasserstein distance {key}", wasserstein_distance(real[:, num], fake[:, num]))

            if step % 200 == 0:
                G.eval()
                G.train()

                fig, axs = plt.subplots(2, 3)
                fig.set_size_inches(20, 20)
                for i, j in enumerate(keys):
                    mi = np.minimum(real[:, i].min(), fake[:, i].min())
                    ma = np.maximum(real[:, i].max(), fake[:, i].max())
                    bins = np.linspace(mi, ma, 100)
                    axs.flatten()[i].hist(real[:, i], bins, alpha=.5)
                    axs.flatten()[i].hist(fake[:, i], bins, alpha=.5)
                    axs.flatten()[i].set_title(j)
                plt.legend(['real'])
                filename_hist = f"hist_epoch_{epoch}_{step:06d}.png"
                plt.savefig(filename_hist)
                plt.clf()
                mlflow.log_artifact(filename_hist)
                os.remove(filename_hist)

        mlflow.pytorch.log_model(G, f"Ge_{epoch}")

mlflow.end_run()