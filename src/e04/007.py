import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
import datetime
import os, sys
import matplotlib.pyplot as plt
import mlflow
import mlflow.pytorch
from scipy.stats import wasserstein_distance
from WrapperDatasetGenerator import WrapperDatasetGenerator
from HDF5DatasetGenerator import HDF5DatasetGenerator

repo_pwd = __file__
for i in range(3):
    repo_pwd = os.path.dirname(repo_pwd)

mlflow.set_tracking_uri(os.path.join(repo_pwd,'mlruns'))

DEVICE = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
print(DEVICE)

file = os.path.join(repo_pwd,'data/structured/')

# Batch size during training
batch_size = 1024

# Number of channels - generated parameters
nc = 6

# Size of z latent vector (i.e. size of generator input)
n_noise = 1600

# Number of training epochs
num_epochs = 1

# number of params
n_params = 4
# Learning rate for optimizers
lr = 0.0002

# Beta1 hyperparam for Adam optimizers
beta1 = 0.35

generator = HDF5DatasetGenerator(
    os.path.join(file,'DISP_0_ANGLE_0','HDF5','a1TrainGenerator.hdf5'),
    500000)
ds = generator.generator()
real_2_hist = next(ds)
generator.close()
del generator

param_2_chart = torch.tensor([[0.,0.,0.,.1]]).to(DEVICE)
keys = ['E', 'X', 'Y', 'dX', 'dY', 'dZ']
min_max_keys = {
    'E':[-1,3],
    'X':[-2.5,2.5],
    'Y':[-2.5,2.5],
    'dX':[-3,3],
    'dY':[-3,3],
    'dZ':[-.2,.1]
}
class Discriminator(nn.Module):

    def __init__(self, in_channel=1, input_size=784, condition_size=10, num_classes=1):
        super(Discriminator, self).__init__()
        self.fc = nn.Sequential(
            nn.Linear(nc+n_params, 2048),
            nn.LayerNorm(2048),
            nn.ReLU(),
            nn.Linear(2048, 4096),
            nn.LayerNorm(4096),
            nn.ReLU(),
            nn.Linear(4096, 4096),
            nn.LayerNorm(4096),
            nn.Sigmoid(),
            nn.Linear(4096, 1),
            nn.Sigmoid()
        )

    def forward(self, x):
        y_ = self.fc(x)
        return y_


class Generator(nn.Module):

    def __init__(self, input_size=100, condition_size=10):
        super(Generator, self).__init__()
        self.dropout_p = 0.002
        self.fc = nn.Sequential(
            nn.Linear(input_size+n_params, 2048),
            nn.Dropout(p=self.dropout_p),
            nn.Tanh(),
            nn.Linear(2048,2048),
            nn.Tanh(),
            nn.Dropout(p=self.dropout_p),
            nn.Linear(2048, 6144),
            nn.ReLU(),
            nn.Linear(6144, nc)
        )

    def forward(self, x):
        y = self.fc(x)
        return y

generator = WrapperDatasetGenerator(file,batch_size)
D = Discriminator().to(DEVICE)
G = Generator(n_noise).to(DEVICE)
D_opt = torch.optim.RMSprop(D.parameters(), lr=0.0005)
G_opt = torch.optim.RMSprop(G.parameters(), lr=0.0005)
step = 0
g_step = 0

D_labels = torch.ones([batch_size, 1]).to(DEVICE) # Discriminator Label to real
D_fakes = torch.zeros([batch_size, 1]).to(DEVICE) # Discriminator Label to fake
mlflow.set_experiment("GAN-experiments-multiple-dist_e04")

def join_p(x,xp):
    return torch.cat([x,xp.repeat(x.shape[0],1)],1)

with mlflow.start_run():
    for epoch in range(num_epochs):
        ds = generator.generator()
        # For each batch in the dataloader
        for step, (params_raw, data_raw) in enumerate(ds):
            data = torch.tensor(data_raw)
            params = torch.tensor(params_raw)
            # Training Discriminator
            x = data.to(DEVICE)
            xp = params.to(DEVICE)

            x_outputs = D(join_p(x,xp))

            z = torch.randn(batch_size, n_noise).to(DEVICE)
            z_outputs = D(join_p(G(join_p(z,xp)),xp))
            D_x_loss = torch.mean(x_outputs)
            D_z_loss = torch.mean(z_outputs)

            D_loss = D_z_loss - D_x_loss

            D.zero_grad()
            D_loss.backward()
            D_opt.step()
            # Parameter(Weight) Clipping for K-Lipshitz constraint
            for p in D.parameters():
                p.data.clamp_(-0.01, 0.01)

            if step % 1 == 0:
                g_step += 1
                # Training Generator
                z = torch.randn(batch_size, n_noise).to(DEVICE)
                z_outputs = D(join_p(G(join_p(z,xp)),xp))
                cost_var = torch.pow(10., torch.randint(-20, -16, (1,))).to(DEVICE)
                G_loss = -torch.mean(z_outputs) - cost_var

                D.zero_grad()
                G.zero_grad()
                G_loss.backward()
                G_opt.step()

            if step % 100 == 0:
                print('Epoch: {}/{}, Step: {}, D Loss: {}, G Loss: {}'.format(epoch, num_epochs, step, D_loss.item(),
                                                                              G_loss.item()))

            if step % 2000 == 0:
                G.eval()
                G.train()

                fig, axs = plt.subplots(2, 3)
                fig.set_size_inches(20, 20)
                temp = []
                for _ in range(500):
                    z = torch.randn(1000, n_noise).to(DEVICE)
                    fake = G(join_p(z,param_2_chart)).to(torch.device("cpu")).detach().numpy()
                    temp.append(fake)
                fake = np.concatenate(temp, axis=0)
                for i, j in enumerate(keys):
                    mi = min_max_keys[j][0]
                    ma = min_max_keys[j][1]
                    bins = np.linspace(mi, ma, 300)
                    axs.flatten()[i].hist(real_2_hist[:, i], bins, alpha=.5)
                    axs.flatten()[i].hist(fake[:, i], bins, alpha=.5)
                    axs.flatten()[i].set_title(j)
                plt.legend(['real'])
                filename_hist = f"hist_epoch_{epoch}_{step:06d}.png"
                plt.savefig(filename_hist)
                plt.clf()
                plt.close('all')
                mlflow.log_artifact(filename_hist)
                os.remove(filename_hist)

            if step % 25000 == 0 and step != 0:
                mlflow.pytorch.log_model(G, f"Ge_step_{step}")
            if step == 100000:
                sys.exit()

mlflow.end_run()