import h5py
import numpy as np

class HDF5DatasetGenerator:
    def __init__(self, dbPath, batchSize):
        self.batchSize = batchSize
        self.db = h5py.File(dbPath,'r')
        self.numParticles = self.db["labels"].shape[0]
        #self.indices = np.arange(self.numParticles)
        #np.random.shuffle(self.indices)
        self.stats = self.db["stats"][:,:]
        self.scaler = np.array([[10,10,10,.1,.1,10]])
    def generator(self):
        for i in range(0, self.numParticles-self.batchSize, self.batchSize):
            #ind = sorted(self.indices[i: i + self.batchSize])
            particles = self.db["particles"][i: i + self.batchSize]
            particles = (particles - self.db["stats"][0,:])/self.db["stats"][1,:]
            labels = self.db["labels"][i: i + self.batchSize]
            yield (particles[:,[0,1,2,5,6,7]]).astype(np.float32)

    def __len__(self):
        return self.numParticles
    def close(self):
        self.db.close()